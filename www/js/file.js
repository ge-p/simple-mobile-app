function onDeviceStorage(){
                var video = $("video source").attr('src');
                var audio = $("audio source").attr('src');
                var image = $(".images").attr('src');
                var file_path;
                var connection = checkConnection();
                if(connection == 'none'){
                    alert("You must be connected to internet!");
                    $(".btnShow").attr("disabled",false);
                    $(".btnShow").text("Show");
                }
                else{
                    downloadVideo(video);
                    downloadAudio(audio);
                    downloadImage(image);  
                }

                
}

function checkConnection() {
    var networkState = navigator.network.connection.type;
    return networkState;
}

function downloadVideo(video){
                    window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, function success(fileSystem){

                        fileSystem.root.getDirectory("files",{create:true},function(d){
                            fileSystem.root.getFile("bunny.mp4", {create: true}, function(fileEntry){
                                var fileTransfer = new FileTransfer();
                                var uri = encodeURI(video);
                                var filePath = fileEntry.toURL();
                                fileTransfer.download(uri, filePath, function(entry) {
                                        console.log("download complete 2: " + filePath);
                                        $(".testing label").text("Video Downloaded!");
                                        $(".btnShow").attr("disabled",false);
                                        $(".btnShow").text("Show");
                                    },
                                    function(error) {
                                        console.log("download error source 2 " + error.source);
                                        console.log("download error target 2 " + error.target);
                                        console.log("upload error code 2 " + error.code);
                                    },
                                    false,
                                    {
                                        headers: {
                                            "Authorization": "Basic dGVzdHVzZXJuYW1lOnRlc3RwYXNzd29yZA=="
                                        }
                                    }
                                );
                            }, 
                            function error(err){
                                console.log("err "+err.code);
                            });
                        },
                        function error(error){
                            alert("Erro "+error.code);
                        });

                    },
                    function error(error){
                        alert(error.code);
                    });
}

function downloadAudio(audio){
                    window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, function success(fileSystem){

                        fileSystem.root.getDirectory("files",{create:true},function(d){
                            fileSystem.root.getFile("hello.mp3", {create: true}, function(fileEntry){
                                var fileTransfer = new FileTransfer();
                                var uri = encodeURI(audio);
                                var filePath = fileEntry.toURL();
                                fileTransfer.download(uri, filePath, function(entry) {
                                        console.log("download complete 2: " + filePath);
                                        $(".testing label").text("Audio Downloaded!");
                                    },
                                    function(error) {
                                        console.log("download error source 2 " + error.source);
                                        console.log("download error target 2 " + error.target);
                                        console.log("upload error code 2 " + error.code);
                                    },
                                    false,
                                    {
                                        headers: {
                                            "Authorization": "Basic dGVzdHVzZXJuYW1lOnRlc3RwYXNzd29yZA=="
                                        }
                                    }
                                );
                            }, 
                            function error(err){
                                console.log("err "+err.code);
                            });
                        },
                        function error(error){
                            alert("Erro "+error.code);
                        });

                    },
                    function error(error){
                        alert(error.code);
                    });
}

function downloadImage(image){
                    window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, function success(fileSystem){

                        fileSystem.root.getDirectory("files",{create:true},function(d){
                            fileSystem.root.getFile("iron-man-3-miyens-blog-400x288.jpg", {create: true}, function(fileEntry){
                                var fileTransfer = new FileTransfer();
                                var uri = encodeURI(image);
                                var filePath = fileEntry.toURL();
                                fileTransfer.download(uri, filePath, function(entry) {
                                        console.log("download complete 2: " + filePath);

                                        $(".testing label").text("Image Downloaded!");
                                    },
                                    function(error) {
                                        console.log("download error source 2 " + error.source);
                                        console.log("download error target 2 " + error.target);
                                        console.log("upload error code 2 " + error.code);
                                    },
                                    false,
                                    {
                                        headers: {
                                            "Authorization": "Basic dGVzdHVzZXJuYW1lOnRlc3RwYXNzd29yZA=="
                                        }
                                    }
                                );
                            }, 
                            function error(err){
                                console.log("err "+err.code);
                            });
                        },
                        function error(error){
                            alert("Erro "+error.code);
                        });

                    },
                    function error(error){
                        alert(error.code);
                    });
}